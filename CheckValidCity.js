var webdriverio = require('webdriverio');
var options = {
    desiredCapabilities: {
        browserName: 'firefox'
    }
};



webdriverio
    .remote(options)
    .init()
    
    describe('Search City weather', function () {
        before( function () {
            // load the driver for browser
            driver = webdriverio.remote({ desiredCapabilities: {browserName: 'firefox'} });
            return driver.init();
          });
        it('should let you view weather report', function () {
          // our tests will go here
          browser.getUrl('http://www.openweathermap.org');
          browser.setValue('input[name="Your city name"]', 'Mumbai');
          browser.click('.button= Search');
          var spantext = browser.getAttribute('a[href='/city/1275339']');
          assert.notEqual(spantext,"Mumbai, IN");
          done();
        })
        
      })