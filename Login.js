var webdriverio = require('webdriverio');
var options = {
    desiredCapabilities: {
        browserName: 'firefox'
    }
};

describe('This test case lets you login', function() {
before( function () {
    // load the driver for browser
    driver = webdriverio.remote({ desiredCapabilities: {browserName: 'firefox'} });
    return driver.init();
  });
it('should let you log in', function () {
    browser.url('https://openweathermap.org/');
    browser.setValue('input[id="user_email"]', 'unik.doshi@gmail.com');
    browser.setValue('input[id="user_password"]', 'Abc@12345');
    browser.submitForm('input[class="btn btn-default btn-color"]');
    var pageUrl = browser.getUrl();
    assert.notEqual(pageUrl, 'https://home.openweathermap.org/users/sign_in');
    assert.equal(pageUrl, 'http://testyourlog.in/example/logged-in.html?email=valid%40user.com&password=hunter2');
  })
  after(function() {
    return driver.end();
  });
})